using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public static class Converters
{
    public static Vector3 ConvertToMovement(this Vector2 input)
    {
        return new Vector3(-input.x, 0, input.y);
    }
}
