using UnityEngine;

public static class RayCastUtils
{
    private const float DefaultRayMaxDistance = 1000f;

    public static void TryGetRaycastPosition(out Vector3 hitPosition, Vector3 cursorPosition, LayerMask rayCastLayerMask, Camera camera,
        float defaultRayMaxDistance = DefaultRayMaxDistance)
    {
        hitPosition = default;

        if (TryGetRaycastHit(out var hitData, cursorPosition, rayCastLayerMask, camera, defaultRayMaxDistance))
        {
            hitPosition = hitData.point;
        }
    }

    public static bool TryGetGameObject(out GameObject gameObject, Vector3 cursorPosition, LayerMask rayCastLayerMask, Camera camera,
        float defaultRayMaxDistance = DefaultRayMaxDistance)
    {
        gameObject = default;

        if (TryGetRaycastHit(out var hitData, cursorPosition, rayCastLayerMask, camera, defaultRayMaxDistance))
        {
            gameObject = hitData.collider.gameObject;

            if (gameObject != null)
            {
                return true;
            }
        }

        return false;
    }

    public static bool TryGetComponentInParent<T>(out T component, Vector3 cursorPosition, LayerMask rayCastLayerMask, Camera camera,
        float defaultRayMaxDistance = DefaultRayMaxDistance) where T : Component
    {
        component = default;

        if (TryGetRaycastHit(out var hitData, cursorPosition, rayCastLayerMask, camera, defaultRayMaxDistance))
        {
            component = hitData.collider.gameObject.GetComponentInParent<T>();

            if (component != null)
            {
                return true;
            }
        }

        return false;
    }  
    public static bool TryGetComponent<T>(out T component, Vector3 cursorPosition, LayerMask rayCastLayerMask, Camera camera,
        float defaultRayMaxDistance = DefaultRayMaxDistance) where T : Component
    {
        component = default;

        if (TryGetRaycastHit(out var hitData, cursorPosition, rayCastLayerMask, camera, defaultRayMaxDistance))
        {
            component = hitData.collider.gameObject.GetComponent<T>();

            if (component != null)
            {
                return true;
            }
        }

        return false;
    }

    public static bool TryGetComponentInChildren<T>(out T component, Vector3 cursorPosition, LayerMask rayCastLayerMask, Camera camera,
        float defaultRayMaxDistance = DefaultRayMaxDistance) where T : Component
    {
        component = default;

        if (TryGetRaycastHit(out var hitData, cursorPosition, rayCastLayerMask, camera, defaultRayMaxDistance))
        {
            component = hitData.collider.gameObject.GetComponentInChildren<T>();

            if (component != null)
            {
                return true;
            }
        }

        return false;
    }

    public static bool TryGetRaycastHit(out RaycastHit raycastHit, Vector3 cursorPosition, LayerMask rayCastLayerMask, Camera camera,
        float defaultRayMaxDistance = DefaultRayMaxDistance)
    {
        Ray ray = camera.ScreenPointToRay(cursorPosition);
        raycastHit = default;

        if (Physics.Raycast(ray, out var hitData, DefaultRayMaxDistance, rayCastLayerMask))
        {
            raycastHit = hitData;

            return true;
        }

        return false;
    }
}