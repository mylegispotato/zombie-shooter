using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.Windows;
using static InputActions;

[RequireComponent(typeof(CharacterController), typeof(PlayerInput))]
public class PlayerController : MonoBehaviour, IMovementActions, IShootingActions, IInteractionsActions
{
    [SerializeField]
    [Range(5f, 100f)]
    private float playerSpeed;
    [SerializeField]
    private LayerMask shootableLayers;

    private PlayerInput playerInput;
    private InputActions inputActions;
    private CharacterController characterController;
    private bool isMoveHeld;
    private Vector2 movementVector;
    private PlayerViewmodel playerViewmodel;
    private Camera camera;
    private Vector3 shootPosition;
    private Vector2 aimingPosition;

    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
        playerInput = GetComponent<PlayerInput>();
        playerViewmodel = GetComponentInChildren<PlayerViewmodel>();
        camera = GetComponentInChildren<Camera>();
        inputActions = new InputActions();
        inputActions.Movement.SetCallbacks(this);
        inputActions.Shooting.SetCallbacks(this);
        inputActions.Interactions.SetCallbacks(this);
        inputActions.Enable();
    }

    private void Update()
    {
        if (isMoveHeld)
        {
            Debug.Log(movementVector.ConvertToMovement());
            characterController.Move(movementVector.ConvertToMovement() * Time.deltaTime * playerSpeed);
        }
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            movementVector = context.ReadValue<Vector2>();
            isMoveHeld = true;
        }
        else if (context.phase == InputActionPhase.Canceled)
        {
            movementVector = Vector2.zero;
            isMoveHeld = false;
        }
    }

    public void OnShoot(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            RayCastUtils.TryGetRaycastPosition(out shootPosition, aimingPosition, shootableLayers, camera);
            playerViewmodel.transform.LookAt(new Vector3(shootPosition.x, playerViewmodel.transform.position.y, shootPosition.z));
        }
    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        Debug.Log("Interact..!");
    }

    public void OnAimingPosition(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            aimingPosition = context.ReadValue<Vector2>();
        }
    }
}
